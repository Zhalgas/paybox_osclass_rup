<?php
/*
Plugin Name: Paybox Russian Ultimate Payments
Plugin URI: paybox.money
Description: Paybox Russian Ultimate Payments plugin.
Version: 1.0.0
Author: PayBox
Author URI: paybox.money
*/
require_once osc_plugins_path() . osc_plugin_folder(__FILE__) . 'PayboxRPModel.php';
require_once osc_plugins_path() . osc_plugin_folder(__FILE__) . 'PayboxRPController.php';

    function paybox_rupayment_install() {
        PayboxRPModel::newInstance()->install();
    }

    function paybox_rupayment_uninstall() {
        PayboxRPModel::newInstance()->uninstall();
    }

	function paybox_rupayment_menu() {
        osc_add_admin_submenu_page('plugins', __('Paybox RUP', 'paybox_rupayment'), osc_route_admin_url('pbrp-menu'), 'paybox_rupayment_settings', 'administrator');
    }

	function paybox_rupayment_admin() {
        osc_redirect_to(osc_route_admin_url('pbrp-menu'));
    }

    function paybox_rupayment_methods() {
        PayboxRPController::newInstance()->index();
    }

    // Страница в админ панеле
    osc_add_route('pbrp-menu', 'paybox_rupayment/admin', 'paybox_rupayment/admin', osc_plugin_folder(__FILE__).'admin/admin.php');

    // Оплата премиум услуг
    osc_add_route('rupayments-user-payments', 'rupayments/payments', 'rupayments/payments', osc_plugin_folder(__FILE__).'methods.php');
    osc_add_route('rupayments-premium-payments', 'rupayments/premium/payment', 'rupayments/premium/payment', osc_plugin_folder(__FILE__).'methods.php');

    // Оплата групп
    osc_add_route('rupayments-user-membership', 'rupayments/membership$', 'rupayments/membership', osc_plugin_folder(__FILE__).'methods.php', true);
    osc_add_route('rupayments-membership-payments', 'rupayments/membership/payment', 'rupayments/membership/payment', osc_plugin_folder(__FILE__).'methods.php');

    // Оплата баннеров
    osc_add_route('rupayments-banner-pay', 'rupayments/banner-pay/([0-9]+)', 'rupayments/banner-pay/{bid}', osc_plugin_folder(__FILE__).'methods.php');
    osc_add_route('rupayments-banner-payments', 'rupayments/banner-payment', 'rupayments/banner-payment', osc_plugin_folder(__FILE__).'methods.php');

    // Оплата кошелька
    osc_add_route('rupayments-user-pack', 'rupayments/pack$', 'rupayments/pack', osc_plugin_folder(__FILE__).'methods.php', true);
    osc_add_route('rupayments-pack-payments', 'rupayments/pack/payment', 'rupayments/pack/payment', osc_plugin_folder(__FILE__).'methods.php');

    // Оплата товара
    osc_add_route('rupayments-ebuy-purchase', 'rupayments/ebuy-purchase/([0-9]+)', 'rupayments/ebuy-purchase/{bid}', osc_plugin_folder(__FILE__).'methods.php');
    osc_add_route('rupayments-ebuy-payments', 'rupayments/ebuy-payments', 'rupayments/ebuy-payments', osc_plugin_folder(__FILE__).'methods.php');

    osc_add_route('pbrp-result', 'paybox/rupayments/result/', 'paybox/rupayments/result/', osc_plugin_folder(__FILE__).'methods.php');
    osc_add_route('pbrp-payment-after', 'paybox/rupayments/status/(.*)/', 'paybox/rupayments/status/{method}/', osc_plugin_folder(__FILE__).'methods.php');

    osc_register_plugin(osc_plugin_path(__FILE__), 'paybox_rupayment_install');
    osc_add_hook(osc_plugin_path(__FILE__)."_uninstall", 'paybox_rupayment_uninstall');
    osc_add_hook(osc_plugin_path(__FILE__)."_configure", 'paybox_rupayment_admin');
	osc_add_hook('admin_menu_init', 'paybox_rupayment_menu');
	osc_add_hook('paybox_methods', 'paybox_rupayment_methods');
?>