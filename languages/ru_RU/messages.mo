��          �      �       H  -   I     w     �     �     �     �     �     �     �       
     	     /   !    Q  B   V  +   �  +   �  )   �  )     )   E  t   o     �     �          %     A  s   ]                       
   	                                       Congratulations, the plugin is now configured Could not find banner Could not find group Could not find order Could not find pack Could not find product Lifetime Merchant ID Paybox settings Save Secret key Test mode This page is available only to authorized users Project-Id-Version: Paybox
POT-Creation-Date: 2018-10-15 17:20+0600
PO-Revision-Date: 2018-10-15 17:20+0600
Last-Translator: 
Language-Team: 
Language: ru_RU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.1.1
X-Poedit-Basepath: ../..
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: languages
X-Poedit-SearchPath-1: admin/admin.php
 Поздравляем, теперь плагин настроен Не удалось найти баннер Не удалось найти группу Не удалось найти заказ Не удалось найти пакет Не удалось найти товар Время жизни счета (в часах): <span style="color: #727272;">По умолчанию: 24ч</span> ID продавца Настройки PayBox Сохранить Секретный ключ Тестовый режим Данная страница доступна только авторизованным пользователям 