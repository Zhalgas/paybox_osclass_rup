<?php defined('ABS_PATH') or die('Access denied');

class PayboxRPModel extends DAO {
    
    private static $instance;

    public static function newInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self;
        }

        return self::$instance;
    }
    
    function __construct()
    {
       parent::__construct();
    }
    
    
    public function install()
    {
        osc_set_preference('version', '100', 'paybox_rupayment', 'INTEGER');
        osc_set_preference('test_mode', '1', 'paybox_rupayment', 'BOOLEAN');
        osc_set_preference('merchant_id', '0', 'paybox_rupayment', 'INTEGER');
        osc_set_preference('secret_key', '0', 'paybox_rupayment', 'STRING');
        osc_set_preference('lifetime', '0', 'paybox_rupayment', 'INTEGER');
    }
    
    public function uninstall()
    {
        Preference::newInstance()->delete(array('s_section' => 'paybox_rupayment'));
    }

    public function getTransactionLog($transaction_id)
    {
        $this->dao->select('*') ;
        $this->dao->from(DB_TABLE_PREFIX.'t_rupayments_log');
        $this->dao->where('s_code', $transaction_id);
        $result = $this->dao->get();

        if($result) {
            return $result->result();
        }

        return array();
    }
}