<?php

class PayboxRPController
{
    private static $instance;

    const
        PUBLISH = 101,
        SET_PREMIUM = 201,
        PUBLISH_AND_SET_PREMIUM = 202,
        HIGHLIGHT_AND_SET_PREMIUM = 231,
        PUBLISH_AND_HIGHLIGHT_AND_SET_PREMIUM = 232,
        HIGHLIGHT = 301,
        PUBLISH_AND_HIGHLIGHT = 302,
        TO_TOP = 401,
        RENEW = 411,
        WALLET = 501,
        MEMBERSHIP = 601,
        SHOW_IMAGE = 701,
        PUBLISH_AND_SHOW_IMAGE = 702,
        SET_PREMIUM_AND_SHOW_IMAGE = 711,
        PUBLISH_AND_SET_PREMIUM_AND_SHOW_IMAGE = 712,
        HIGHLIGHT_AND_SHOW_IMAGE = 721,
        PUBLISH_AND_HIGHLIGHT_AND_SHOW_IMAGE = 722,
        SET_PREMIUM_AND_HIGHLIGHT_AND_SHOW_IMAGE = 731,
        PUBLISH_AND_SET_PREMIUM_AND_HIGHLIGHT_AND_SHOW_IMAGE = 732,
        PACK_3_IN_1 = 801,
        PUBLISH_AND_PACK_3_IN_1 = 802,
        ACTIVATE_SHOW_IMAGE_AND_PACK_3_IN_1 = 811,
        PUBLISH_AND_ACTIVATE_SHOW_IMAGE_AND_PACK_3_IN_1 = 812,
        BANNER = 901,
        PURCHASE = 1001;

    public static function newInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function index()
    {
        $route = Params::getParam('route');

        switch ($route) {
            case 'rupayments-user-payments':
                $this->payments_page();
                break;

            case 'rupayments-premium-payments':
                $this->premium_payment();
                break;

            case 'rupayments-user-membership':
                $this->membership_page();
                break;

            case 'rupayments-membership-payments':
                $this->membership_payment();
                break;

            case 'rupayments-banner-pay':
                $this->banner_page();
                break;

            case 'rupayments-banner-payments':
                $this->banner_payment();
                break;

            case 'rupayments-user-pack':
                $this->pack_page();
                break;

            case 'rupayments-pack-payments':
                $this->pack_payment();
                break;

            case 'rupayments-ebuy-purchase':
                $this->ebuy_page();
                break;

            case 'rupayments-ebuy-payments':
                $this->ebuy_payment();
                break;

            case 'pbrp-result':
                $this->result();
                break;

            case 'pbrp-payment-after':
                $this->payment_after();
                break;
        }
    }

    /**
     * Страница выбора платежных систем для оплаты премиум услуг
     */
    public function payments_page()
    {
        $item = Item::newInstance()->findByPrimaryKey(Params::getParam('itemId'));

        if (osc_get_preference('allow_itempost_form', 'rupayments') == 1) {
            if (osc_get_preference('pay_per_show_image_status', 'rupayments')) {
                ModelRUpayments::newInstance()->setImageShow(Params::getParam('itemId'), 0);
            }

            ModelRUpayments::newInstance()->isPublishPaymentNeedednext($item);
        }

        $product_type = Params::getParam('productType');

        if ($product_type == '001') {
            if (osc_get_preference('pay_per_show_image_status', 'rupayments')) {
                ModelRUpayments::newInstance()->setImageShow(Params::getParam('itemId'), 0);
            }

            rupayments_js_redirect_to(osc_base_url());
        }

        if ($item = Item::newInstance()->findByPrimaryKey(Params::getParam('itemId'))) {
            $item_id = Params::getParam('itemId');

            if (osc_get_preference('allow_itempost_form', 'rupayments') == 1) {
                $category_id = $item['fk_i_category_id'];
            } else {
                $category_id = Params::getParam('categoryId');
            }

            $url_back = osc_route_url(Params::getParam('url_back'), array('itemId' => $item_id));
            $lable = '';
            $image = '';

            $show_url_back = false;

            if (Params::getParam('url_back') == 'rupayments-publish') {
                $show_url_back = true;
            }

            if (mb_stristr(Params::getParam('url_back'), osc_base_url())) {
                $show_url_back = true;
                $url_back = Params::getParam('url_back');
            }

            $price = ModelRUpayments::newInstance()->calculatePrice($product_type, $category_id, $item);

            $wallet = ModelRUpayments::newInstance()->getWallet(osc_logged_user_id());
            $amount = isset ($wallet['i_amount']) ? $wallet['i_amount'] : 0;

            if ($amount != 0) {
                if (osc_get_preference("currency", "rupayments") == 'BTC') {
                    $amount2 = number_format($amount, 8);
                } else {
                    $amount2 = round($amount, 2);
                }

                $credit_msg = sprintf(__('Your current credit is %s %s', 'rupayments'), $amount2, osc_get_preference("currency", "rupayments"));
            } else {
                $credit_msg = __('Your wallet is empty.', 'rupayments');
            }

            $site_title = mb_eregi_replace("http://", "", osc_base_url());
            $site_title = " " . mb_eregi_replace("/", "", $site_title);

            $mnu = osc_base_url();
            $description = "";

            if ($product_type == self::PUBLISH) {
                $description = __('Publish Item', 'rupayments');
                $lable = __('Publish fee for item %d', 'rupayments');
                $s2CoType = "4";
                $image = '<span class="mdi mdi-near-me mdi-36px"></span>';
            } else if ($product_type == self::SET_PREMIUM) {
                if (!ModelRUpayments::newInstance()->getIsPublishPaymentNeeded($item_id) || ModelRUpayments::newInstance()->publishFeeIsPaid($item_id)) {
                    $description = __('Make Item Premium', 'rupayments');
                    $lable = __('Premium fee for item %d', 'rupayments');
                    $image = '<span class="mdi mdi-star mdi-36px"></span>';
                } else {

                    $description = __('Publish and Make Item Premium', 'rupayments');
                    $lable = __('Publish and Premium fee for item %d', 'rupayments');
                    $image = '<span class="mdi mdi-near-me mdi-36px"></span><span class="mdi mdi-star mdi-36px"></span>';
                }

                $s2CoType = "1";
            } else if ($product_type == self::HIGHLIGHT) {
                if (!ModelRUpayments::newInstance()->getIsPublishPaymentNeeded($item_id) || ModelRUpayments::newInstance()->publishFeeIsPaid($item_id)) {
                    $description = __('Make Item Highlighted', 'rupayments');
                    $lable = __('Highlighted fee for item %d', 'rupayments');
                    $image = '<span class="mdi mdi-format-color-fill mdi-36px"></span>';
                } else {

                    $description = __('Publish and Make Item Highlighted', 'rupayments');
                    $lable = __('Publish and Highlighted fee for item %d', 'rupayments');
                    $image = '<span class="mdi mdi-near-me mdi-36px"></span><span class="mdi mdi-format-color-fill mdi-36px"></span>';
                }

                $s2CoType = "3";
            } else if ($product_type == self::SHOW_IMAGE) {

                if (!ModelRUpayments::newInstance()->getIsPublishPaymentNeeded($item_id) || ModelRUpayments::newInstance()->publishFeeIsPaid($item_id)) {
                    $description = __('Show Image', 'rupayments');
                    $lable = __('Show Image fee for item %d', 'rupayments');
                    $image = '<span class="mdi mdi-image mdi-36px"></span>';
                } else {

                    $description = __('Publish and Show Image', 'rupayments');
                    $lable = __('Publish and Show Image fee for item %d', 'rupayments');
                    $image = '<span class="mdi mdi-near-me mdi-36px"></span><span class="mdi mdi-image mdi-36px"></span>';
                }

                $s2CoType = "10";
            } else if ($product_type == self::SET_PREMIUM_AND_SHOW_IMAGE) {

                if (!ModelRUpayments::newInstance()->getIsPublishPaymentNeeded($item_id) || ModelRUpayments::newInstance()->publishFeeIsPaid($item_id)) {
                    $description = __('Make Item Premium and Show Image', 'rupayments');
                    $lable = __('Make Item Premium and Show Image fee for item %d', 'rupayments');
                    $image = '<span class="mdi mdi-star mdi-36px"></span><span class="mdi mdi-image mdi-36px"></span>';
                } else {

                    $description = __('Publish, Make Item Premium and Show Image', 'rupayments');
                    $lable = __('Publish, Make Item Premium and Show Image fee for item %d', 'rupayments');
                    $image = '<span class="mdi mdi-near-me mdi-36px"></span><span class="mdi mdi-star mdi-36px"></span><span class="mdi mdi-image mdi-36px"></span>';
                }

                $s2CoType = "11";
            } else if ($product_type == self::HIGHLIGHT_AND_SHOW_IMAGE) {
                if (!ModelRUpayments::newInstance()->getIsPublishPaymentNeeded($item_id) || ModelRUpayments::newInstance()->publishFeeIsPaid($item_id)) {
                    $description = __('Make Item Highlighted and Show Image', 'rupayments');
                    $lable = __('Make Item Highlighted and Show Image fee for item %d', 'rupayments');
                    $image = '<span class="mdi mdi-format-color-fill mdi-36px"></span><span class="mdi mdi-image mdi-36px"></span>';
                } else {
                    $description = __('Publish, Make Item Highlighted and Show Image', 'rupayments');
                    $lable = __('Publish, Make Item Highlighted and Show Image fee for item %d', 'rupayments');
                    $image = '<span class="mdi mdi-near-me mdi-36px"><span class="mdi mdi-format-color-fill mdi-36px"></span></span><span class="mdi mdi-image mdi-36px"></span>';
                }
                $s2CoType = "12";

            } else if ($product_type == self::SET_PREMIUM_AND_HIGHLIGHT_AND_SHOW_IMAGE) {
                if (!ModelRUpayments::newInstance()->getIsPublishPaymentNeeded($item_id) || ModelRUpayments::newInstance()->publishFeeIsPaid($item_id)) {
                    $description = __('Make Item Premium, Make Item Highlighted and Show Image', 'rupayments');
                    $lable = __('Make Item Premium, Make Item Highlighted and Show Image fee for item %d', 'rupayments');
                    $image = '<span class="mdi mdi-star mdi-36px"></span><span class="mdi mdi-format-color-fill mdi-36px"></span><span class="mdi mdi-image mdi-36px"></span>';
                } else {

                    $description = __('Publish, Make Item Premium, Make Item Highlighted and Show Image', 'rupayments');
                    $lable = __('Publish, Make Item Premium, Make Item Highlighted and Show Image fee for item %d', 'rupayments');
                    $image = '<span class="mdi mdi-near-me mdi-36px"></span><span class="mdi mdi-star mdi-36px"></span><span class="mdi mdi-format-color-fill mdi-36px"></span><span class="mdi mdi-image mdi-36px"></span>';
                }

                $s2CoType = "13";
            } else if ($product_type == self::PACK_3_IN_1) {
                if (!ModelRUpayments::newInstance()->getIsPublishPaymentNeeded($item_id) || ModelRUpayments::newInstance()->publishFeeIsPaid($item_id)) {
                    $description = __('Apply: Pack 3-in-1', 'rupayments');
                    $lable = __('Apply: Pack 3-in-1 fee for item %d', 'rupayments');
                    $image = '<span class="mdi mdi-certificate mdi-36px"></span>';
                } else {
                    $description = __('Publish and apply: Pack 3-in-1', 'rupayments');
                    $lable = __('Publish and apply: Pack 3-in-1 fee for item %d', 'rupayments');
                    $image = '<span class="mdi mdi-near-me mdi-36px"></span><span class="mdi mdi-certificate mdi-36px"></span>';
                }

                $s2CoType = "14";
            } else if ($product_type == self::ACTIVATE_SHOW_IMAGE_AND_PACK_3_IN_1) {

                if (!ModelRUpayments::newInstance()->getIsPublishPaymentNeeded($item_id) || ModelRUpayments::newInstance()->publishFeeIsPaid($item_id)) {
                    $description = __('Activate Show Image and apply: Pack 3-in-1', 'rupayments');
                    $lable = __('Activate Show Image and apply: Pack 3-in-1 fee for item %d', 'rupayments');
                    $image = '<span class="mdi mdi-image mdi-36px"></span><span class="mdi mdi-certificate mdi-36px"></span>';
                } else {

                    $description = __('Publish, activate Show Image and apply: Pack 3-in-1', 'rupayments');
                    $lable = __('Publish, activate Show Image and apply: Pack 3-in-1 fee for item %d', 'rupayments');
                    $image = '<span class="mdi mdi-near-me mdi-36px"></span><span class="mdi mdi-image mdi-36px"></span><span class="mdi mdi-certificate mdi-36px"></span>';
                }

                $s2CoType = "15";
            } else if ($product_type == self::HIGHLIGHT_AND_SET_PREMIUM) {
                $sAddon = "";

                if (!ModelRUpayments::newInstance()->getIsPublishPaymentNeeded($item_id) || ModelRUpayments::newInstance()->publishFeeIsPaid($item_id)) {
                    $description = __('Make Item Highlighted and Premium', 'rupayments') . $sAddon;
                    $lable = __('Premium and Highlighted fee for item %d', 'rupayments');
                    $image = '<span class="mdi mdi-star mdi-36px"></span><span class="mdi mdi-format-color-fill mdi-36px"></span>';
                } else {
                    $description = __('Publish and Make Item Highlighted and Premium', 'rupayments') . $sAddon;
                    $lable = __('Publish, Premium and Highlighted fee for item %d', 'rupayments');
                    $image = '<span class="mdi mdi-near-me mdi-36px"></span><span class="mdi mdi-star mdi-36px"></span><span class="mdi mdi-format-color-fill mdi-36px"></span>';
                }
                $s2CoType = "9";

            } else if ($product_type == self::TO_TOP) {
                $description = __('Move Item to TOP', 'rupayments');
                $lable = __('Move to TOP fee for item %d', 'rupayments');
                $s2CoType = "2";
                $image = '<span class="mdi mdi-arrow-up-thick mdi-36px"></span>';
            } else if ($product_type == self::RENEW) {
                $description = __('Renew Item', 'rupayments');
                $lable = __('Renew fee for item %d', 'rupayments');
                $s2CoType = "21";
                $image = '<span class="mdi mdi-autorenew mdi-36px"></span>';
            }
        }

        $this->view('pages/payments.php', array(
            'description' => $description,
            'lable' => $lable,
            'image' => $image,
            'product_type' => $product_type,
            'show_url_back' => $show_url_back,
            'url_back' => $url_back,
            'item_id' => $item_id,
            'price' => $price,
            'credit_msg' => $credit_msg,
            'wallet' => $wallet,
            'item' => $item,
            's2CoType' => $s2CoType,
            'site_title' => $site_title,
            'mnu' => $mnu
        ));
    }

    /**
     * Оплата премиум услуг
     */
    public function premium_payment()
    {
        $product_type = Params::getParam('product_type');
        $item_id = Params::getParam('item_id');
        $email = Params::getParam('email');
        $desc = Params::getParam('description');
        $item = Item::newInstance()->findByPrimaryKey($item_id);

        if (!$item) {
            osc_add_flash_error_message(__('Could not find product', 'paybox_rupayments'));
            rupayments_js_redirect_to(osc_search_category_url());
            return;
        }

        $price = Params::getParam('price');
        $order = $this->createOrder($transaction_id = 'Ya'. time(), $email, $price, $product_type, $item_id);

        if (!$order) {
            osc_add_flash_error_message(__('Could not find order', 'paybox_rupayments'));
            rupayments_js_redirect_to(osc_search_category_url());
            return;
        }

        $this->payment($price, $desc, $order);
    }

    /**
     * Страница выбора платежных систем для оплаты группы
     */
    public function membership_page()
    {
        if (!osc_logged_user_id()) {
            osc_add_flash_error_message(__('This page is available only to authorized users', 'paybox_rupayments'));
            rupayments_js_redirect_to(osc_search_category_url());
            return;
        }

        $memberships = ModelRUpayments::newInstance()->getUserGroups();

        $user = User::newInstance()->findByPrimaryKey(osc_logged_user_id());
        $user_membership = ModelRUpayments::newInstance()->getUserMembership(osc_logged_user_id());
        $wallet = ModelRUpayments::newInstance()->getWallet(osc_logged_user_id());
        $sSite_title = mb_eregi_replace ( "http://", "", osc_base_url() );
        $sSite_title = "From ".mb_eregi_replace ( "/", "", $sSite_title );

        $this->view('pages/membership.php', array(
            'memberships' => $memberships,
            'user' => $user,
            'user_membership' => $user_membership,
            'wallet' => $wallet,
            'sSite_title' => $sSite_title
        ));
    }

    /**
     * Оплата пакета для аккаунта
     */
    public function membership_payment()
    {
        $product_type = self::MEMBERSHIP;
        $group_id = Params::getParam('group_id');
        $email = Params::getParam('email');
        $desc = Params::getParam('description');
        $group = ModelRUpayments::newInstance()->getUserGroup($group_id);

        if (!$group) {
            osc_add_flash_error_message(__('Could not find group', 'paybox_rupayments'));
            rupayments_js_redirect_to(osc_search_category_url());
            return;
        }

        $price = $group['f_group_price'];
        $order = $this->createOrder($transaction_id = 'Ya'. time(), $email, $price, $product_type, $group_id);

        if (!$order) {
            osc_add_flash_error_message(__('Could not find order', 'paybox_rupayments'));
            rupayments_js_redirect_to(osc_search_category_url());
            return;
        }

        $this->payment($price, $desc, $order);
    }

    /**
     * Страница выбора платежных систем для оплаты баннера
     */
    public function banner_page()
    {
        if (!osc_logged_user_id()) {
            osc_add_flash_error_message(__('This page is available only to authorized users', 'paybox_rupayments'));
            rupayments_js_redirect_to(osc_search_category_url());
            return;
        }

        $user = User::newInstance()->findByPrimaryKey(osc_logged_user_id());
        $check_banner_pay = ModelRUpayments::newInstance()->checkUserBannerPay(Params::getParam('bid'), osc_logged_user_id());
        $get_banner = ModelRUpayments::newInstance()->getUserBannerPublish(Params::getParam('bid'), osc_logged_user_id());
        $wallet = ModelRUpayments::newInstance()->getWallet(osc_logged_user_id());

        $amount = isset($wallet['i_amount']) ? $wallet['i_amount'] : 0;

        if ($amount != 0) {
            if (osc_get_preference ( "currency", "rupayments" )=='BTC'){
                $amount2 = number_format($amount,8);
            } else {
                $amount2 = round($amount,2);
            }

            $credit_msg = sprintf(__('Your current credit is %s %s', 'rupayments'), $amount2, osc_get_preference ( "currency", "rupayments" ));
        } else {
            $credit_msg = __('Your wallet is empty.', 'rupayments');
        }

        $sSite_title = mb_eregi_replace ( "http://", "", osc_base_url() );
        $sSite_title = "From ".mb_eregi_replace ( "/", "", $sSite_title );

        if (!$check_banner_pay) {
            ob_get_clean();
            osc_add_flash_error_message(__('Error: You can\'t pay for the banner!', 'rupayments'));
            osc_redirect_to(osc_route_url('rupayments-user-banners'));
        }

        $this->view('pages/banner.php', array(
            'get_banner' => $get_banner,
            'user' => $user,
            'credit_msg' => $credit_msg,
            'wallet' => $wallet,
            'sSite_title' => $sSite_title
        ));
    }

    /**
     * Оплата баннера
     */
    public function banner_payment()
    {
        $product_type = self::BANNER;
        $banner_id = Params::getParam('banner_id');
        $desc = Params::getParam('description');
        $banner = ModelRUpayments::newInstance()->getUserBannerPublish($banner_id, osc_logged_user_id());

        if (!$banner) {
            osc_add_flash_error_message(__('Could not find banner', 'paybox_rupayments'));
            rupayments_js_redirect_to(osc_search_category_url());
            return;
        }

        $price = $banner['i_banner_budget'];
        $order = $this->createOrder($transaction_id = 'Ya'. time(), osc_logged_user_email(), $price, $product_type, $banner_id);

        if (!$order) {
            osc_add_flash_error_message(__('Could not find order', 'paybox_rupayments'));
            rupayments_js_redirect_to(osc_search_category_url());
            return;
        }

        $this->payment($price, $desc, $order);
    }

    /**
     * Страница выбора платежных систем для оплаты кошелька
     */
    public function pack_page()
    {
        if ( !osc_logged_user_id() ) {
            osc_add_flash_error_message(__('This page is available only to authorized users', 'paybox_rupayments'));
            rupayments_js_redirect_to(osc_search_category_url());
            return;
        }

        $packs = ModelRUpayments::newInstance()->getPacks();

        $user = User::newInstance()->findByPrimaryKey(osc_logged_user_id());
        $wallet = ModelRUpayments::newInstance()->getWallet(osc_logged_user_id());
        $amount = isset($wallet['i_amount'])?$wallet['i_amount']:0;

        if ($amount != 0) {
            $amount = round($amount,2);
            $credit_msg = sprintf(__('Your current credit is %s %s', 'rupayments'), $amount, osc_get_preference ( "currency", "rupayments" ));
        } else {
            $credit_msg = __('Your wallet is empty. Buy some credits.', 'rupayments');
        }

        $sSite_title = mb_eregi_replace ( "http://", "", osc_base_url() );
        $sSite_title = "From ".mb_eregi_replace ( "/", "", $sSite_title );

        $this->view('pages/pack.php', array(
            'packs' => $packs,
            'user' => $user,
            'credit_msg' => $credit_msg,
            'wallet' => $wallet,
            'sSite_title' => $sSite_title
        ));
    }

    /**
     * Оплата пакета для кошелька
     */
    public function pack_payment()
    {
        $product_type = self::WALLET;
        $desc = Params::getParam('description');
        $pack_id = Params::getParam('item_id');
        $pack = ModelRUpayments::newInstance()->getPack($pack_id);

        if (!$pack) {
            osc_add_flash_error_message(__('Could not find pack', 'paybox_rupayments'));
            rupayments_js_redirect_to(osc_search_category_url());
            return;
        }

        $price = $pack['f_pack_amount'];
        $order = $this->createOrder($transaction_id = 'Ya'. time(), osc_logged_user_email(), $price, $product_type, $pack_id);

        if (!$order) {
            osc_add_flash_error_message(__('Could not find order', 'paybox_rupayments'));
            rupayments_js_redirect_to(osc_search_category_url());
            return;
        }

        $this->payment($price, $desc, $order);
    }

    /**
     * Страница выбора платежных систем для оплаты товара
     */
    public function ebuy_page()
    {
        if (!osc_logged_user_id()) {
            osc_add_flash_error_message(__('This page is available only to authorized users', 'paybox_rupayments'));
            rupayments_js_redirect_to(osc_search_category_url());
            return;
        }

        $user = User::newInstance()->findByPrimaryKey(osc_logged_user_id());
        $ebuy_item = ModelRUpayments::newInstance()->getEbuyItem(Params::getParam('bid'));
        $item = Item::newInstance()->findByPrimaryKey($ebuy_item['i_item_id']);
        $check_ebuy_pay = ModelRUpayments::newInstance()->checkEbuyPay(Params::getParam('bid'), osc_logged_user_id());
        $wallet = ModelRUpayments::newInstance()->getWallet(osc_logged_user_id());

        $amount = isset ($wallet['i_amount']) ? $wallet['i_amount'] : 0;

        if ($amount != 0) {

            if (osc_get_preference ("currency", "rupayments" ) == 'BTC') {
                $amount2=number_format($amount,8);
            } else {
                $amount2=round($amount,2);
            }

            $credit_msg = sprintf(__('Your current credit is %s %s', 'rupayments'), $amount2, osc_get_preference ( "currency", "rupayments" ));
        } else {
            $credit_msg = __('Your wallet is empty.', 'rupayments');
        }

        $sSite_title = mb_eregi_replace ( "http://", "", osc_base_url() );
        $sSite_title = "From ".mb_eregi_replace ( "/", "", $sSite_title );

        if(!$check_ebuy_pay || !$ebuy_item) {
            ob_get_clean();
            osc_add_flash_error_message(__('Error: You can\'t pay for the item!', 'rupayments'));
            osc_redirect_to(osc_route_url('rupayments-user-ebuy-deals'));
        }

        $this->view('pages/ebuy.php', array(
            'item' => $item,
            'ebuy_item' => $ebuy_item,
            'user' => $user,
            'credit_msg' => $credit_msg,
            'wallet' => $wallet,
            'sSite_title' => $sSite_title
        ));
    }

    /**
     * Оплата товара
     */
    public function ebuy_payment()
    {
        $product_type = self::PURCHASE;
        $desc = Params::getParam('description');
        $item_id = Params::getParam('item_id');
        $item = ModelRUpayments::newInstance()->getEbuyItem($item_id);

        if (!$item) {
            osc_add_flash_error_message(__('Could not find product', 'paybox_rupayments'));
            rupayments_js_redirect_to(osc_search_category_url());
            return;
        }

        $price = $item['s_item_price'];
        $order = $this->createOrder($transaction_id = 'Ya'. time(), osc_logged_user_email(), $price, $product_type, $item_id);

        if (!$order) {
            osc_add_flash_error_message(__('Could not find order', 'paybox_rupayments'));
            rupayments_js_redirect_to(osc_search_category_url());
            return;
        }

        $this->payment($price, $desc, $order);
    }

    /**
     * Создания заказа
     *
     * @param string $transaction_id
     * @param string $email
     * @param integer $price
     * @param integer $product_type
     * @param integer $item_id
     *
     * @return Object
     */
    public function createOrder($transaction_id, $email, $price, $product_type, $item_id)
    {
        ModelRUpayments::newInstance()->saveTransactionId($transaction_id, $email, $price, $product_type, $item_id);

        return ModelRUpayments::newInstance()->getTransaction($transaction_id);
    }

    /**
     * Перенаправление на платежную страницу PayBox
     *
     * @param integer $price
     * @param string $desc
     * @param Object $order
     */
    public function payment($price, $desc, $order)
    {
        $merchant_id = osc_get_preference('merchant_id', 'paybox_rupayment');
        $secret_key = osc_get_preference('secret_key', 'paybox_rupayment');
        $lifetime = osc_get_preference('lifetime', 'paybox_rupayment');

        $array = array(
            'pg_amount'         => (int)$price,
            'pg_description'    => $desc,
            'pg_encoding'       => 'UTF-8',
            'pg_currency'       => $order['s_currency_code'],
            'pg_user_ip'        => $_SERVER['REMOTE_ADDR'],
            'pg_lifetime'       => !empty($lifetime) ? $lifetime * 3600 : 86400,
            'pg_merchant_id'    => $merchant_id,
            'pg_order_id'       => $order['s_transaction_id'],
            'pg_result_url'     => osc_route_url('pbrp-result'),
            'pg_request_method' => 'GET',
            'pg_salt'           => rand(21, 43433),
            'pg_success_url'    => osc_route_url('pbrp-payment-after', array('method' => 'success')),
            'pg_failure_url'    => osc_route_url('pbrp-payment-after', array('method' => 'failure')),
            'pg_user_contact_email' => $order['s_email']
        );

        if (osc_get_preference('test_mode', 'paybox_rupayment') == 1) {
            $array['pg_testing_mode'] = 1;
        }

        $array['pg_sig'] = $this->makeSig('payment.php', $array, $secret_key);

        header('Location: https://api.paybox.money/payment.php?' . http_build_query($array));
    }

    /**
     * Получение результата платежа от PayBox
     */
    public function result()
    {
        $response = array();
        $order_id = Params::getParam('pg_order_id');
        $salt = Params::getParam('pg_salt');
        $result = Params::getParam('pg_result');
        $sig = Params::getParam('pg_sig');
        $secret_key = osc_get_preference('secret_key', 'paybox_rupayment');
        $all_params = Params::getParamsAsArray();

        unset($all_params['method'], $all_params['page'], $all_params['route'], $all_params['pg_sig']);

        // Проверка подписи
        if (!$this->checkSig($sig, '', $all_params, $secret_key)) {
            ob_clean();
            die('Bad signature');
        }

        $order_info = ModelRUpayments::newInstance()->getTransaction($order_id);
        $user_info = User::newInstance()->findByEmail($order_info['s_email']);
        $log = PayboxRPModel::newInstance()->getTransactionLog($order_id);

        if (!$order_info || !$user_info) {
            $response['pg_status'] = 'error';
            $response['pg_error_description'] = (!$order_info) ? 'Не удалось найти заказ' : 'Не удалось найти пользователя';
            $response['pg_salt'] = $salt;
            $response['pg_sig'] = $this->makeSig('index.php', $response, $secret_key);

            ob_clean();
            header('Content-type: text/xml');
            echo $this->createXML($response);
            exit;
        }

        // Проверка если операция по этому заказу уже совершена
        if ($log) {
            $response['pg_status'] = 'ok';
            $response['pg_error_description'] = '';
            $response['pg_salt'] = $salt;
            $response['pg_sig'] = $this->makeSig('index.php', $response, $secret_key);

            ob_clean();
            header('Content-type: text/xml');
            echo $this->createXML($response);
            exit;
        }

        if ($result != 1) {
            $response['pg_status'] = 'failed';
            $response['pg_error_description'] = '';
        } else if (isset($order_info['s_transaction_id'])) {
            $response['pg_status'] = 'ok';
            $response['pg_error_description'] = '';
        } else {
            $response['pg_status'] = 'rejected';
            $response['pg_error_description'] = '';
        }

        $response['pg_salt'] = $salt;
        $response['pg_sig'] = $this->makeSig('index.php', $response, $secret_key);

        if ($response['pg_status'] == 'ok') {
            $this->saveLog($order_info, $user_info['pk_i_id']);
            $this->complete_order($order_info['i_product_type'], $order_info['fk_i_item_id'], $user_info['pk_i_id']);
        }

        ob_clean();

        header('Content-type: text/xml');
        echo $this->createXML($response);
        exit;
    }

    /**
     * Создания XML для ответа
     */
    public function createXML($data)
    {
        $xml = new SimpleXMLElement('<xml/>');

        $response = $xml->addChild('response');
        $response->addChild('pg_salt', $data['pg_salt']);
        $response->addChild('pg_status', $data['pg_status']);
        $response->addChild('pg_error_description', $data['pg_error_description']);
        $response->addChild('pg_sig', $data['pg_sig']);

        return $xml->asXML();
    }

    /**
     * Сохранение логов
     *
     * @param Object $order
     * @param integer $user
     */
    public function saveLog($order, $user)
    {
        $product_type = $order['i_product_type'];
        $order_id = $order['s_transaction_id'];
        $amount = $order['f_amount'];
        $currency = $order['s_currency_code'];
        $email = $order['s_email'];
        $item_id = $order['fk_i_item_id'];

        $messages = array(
            self::PUBLISH => __('Publish Item', 'rupayments'),
            self::SET_PREMIUM => __('Make Item Premium', 'rupayments'),
            self::PUBLISH_AND_SET_PREMIUM => __('Publish and Make Item Premium', 'rupayments'),
            self::HIGHLIGHT_AND_SET_PREMIUM => __('Make Item Highlighted and Premium', 'rupayments'),
            self::PUBLISH_AND_HIGHLIGHT_AND_SET_PREMIUM => __('Publish and Make Item Highlighted and Premium', 'rupayments'),
            self::HIGHLIGHT => __('Make Item Highlighted', 'rupayments'),
            self::PUBLISH_AND_HIGHLIGHT => __('Publish and Make Item Highlighted', 'rupayments'),
            self::TO_TOP => __('Move to TOP', 'rupayments'),
            self::RENEW => __('Renew item', 'rupayments'),
            self::MEMBERSHIP => __('Payment of membership fee: ', 'rupayments'),
            self::SHOW_IMAGE => __('Show Image', 'rupayments'),
            self::PUBLISH_AND_SHOW_IMAGE => __('Publish and Show Image', 'rupayments'),
            self::SET_PREMIUM_AND_SHOW_IMAGE => __('Make Item Premium and Show Image', 'rupayments'),
            self::PUBLISH_AND_SET_PREMIUM_AND_SHOW_IMAGE => __('Publish, Make Item Premium and Show Image', 'rupayments'),
            self::HIGHLIGHT_AND_SHOW_IMAGE => __('Make Item Highlighted and Show Image', 'rupayments'),
            self::PUBLISH_AND_HIGHLIGHT_AND_SHOW_IMAGE => __('Publish, Make Item Highlighted and Show Image', 'rupayments'),
            self::SET_PREMIUM_AND_HIGHLIGHT_AND_SHOW_IMAGE => __('Make Item Premium, Make Item Highlighted and Show Image', 'rupayments'),
            self::PUBLISH_AND_SET_PREMIUM_AND_HIGHLIGHT_AND_SHOW_IMAGE => __('Publish, Make Item Premium, Make Item Highlighted and Show Image', 'rupayments'),
            self::PACK_3_IN_1 => __('Apply: Pack 3-in-1', 'rupayments'),
            self::PUBLISH_AND_PACK_3_IN_1 => __('Publish and Apply: Pack 3-in-1', 'rupayments'),
            self::ACTIVATE_SHOW_IMAGE_AND_PACK_3_IN_1 => __('Activate Show Image and apply: Pack 3-in-1', 'rupayments'),
            self::PUBLISH_AND_ACTIVATE_SHOW_IMAGE_AND_PACK_3_IN_1 => __('Publish, Activate Show Image and apply: Pack 3-in-1', 'rupayments'),
            self::BANNER => __('Payment of banner public fee: ', 'rupayments'),
            self::PURCHASE => __('Payment of Item Purchase: ', 'rupayments'),
        );

        if ($product_type == self::WALLET) {
            $pack = ModelRUpayments::newInstance()->getPack($item_id);
            $pack_name = $pack['f_pack_title'];

            if($pack['f_pack_bonus']) {
                $bonus_amount = $pack['f_pack_bonus'];
                $bonus_text = __(' + Bonus: ', 'rupayments') . $bonus_amount . osc_get_preference('currency', 'rupayments');
            }

            $messages[self::WALLET] = $pack_name . (isset($bonus_text) ? $bonus_text : '');
        }

        if (!key_exists($product_type, $messages)) {
            return;
        }

        ModelRUpayments::newInstance()->saveLog(
            $messages[$product_type],
            $order_id, // transaction id
            $amount, //amount
            $currency, //currency
            $email, // payer's email
            $user, //user
            $item_id, //item
            $product_type, //product type
            'PayBox' //source
        );
    }

    /**
     * @param integer $product_type
     * @param integer $item_id
     * @param integer $user_id
     */
    public function complete_order($product_type, $item_id, $user_id)
    {
        $payment_id = ModelRUpayments::newInstance()->lastId;

        if ($product_type == self::PUBLISH) {
            ModelRUpayments::newInstance()->payPublishFee($item_id, $payment_id);

            if (osc_get_preference('pay_per_show_image_status', 'rupayments') && !ModelRUpayments::newInstance()->checkShowImage($item_id)) {
                ModelRUpayments::newInstance()->setImageShow($item_id, 0);
            }
        } else if ($product_type == self::SET_PREMIUM || $product_type == self::PUBLISH_AND_SET_PREMIUM) {
            if (ModelRUpayments::newInstance()->getIsPublishPaymentNeeded($item_id)) {
                ModelRUpayments::newInstance()->payPublishFee($item_id, $payment_id);
            }

            if(osc_get_preference('pay_per_show_image_status', 'rupayments') && !ModelRUpayments::newInstance()->checkShowImage($item_id)) {
                ModelRUpayments::newInstance()->setImageShow($item_id, 0);
            }

            ModelRUpayments::newInstance()->payPremiumFee($item_id, $payment_id);
        } else if ($product_type == self::HIGHLIGHT || $product_type == self::PUBLISH_AND_HIGHLIGHT) {
            if (ModelRUpayments::newInstance()->getIsPublishPaymentNeeded($item_id)) {
                ModelRUpayments::newInstance()->payPublishFee($item_id, $payment_id);
            }

            if (osc_get_preference('pay_per_show_image_status', 'rupayments') && !ModelRUpayments::newInstance()->checkShowImage($item_id)) {
                ModelRUpayments::newInstance()->setImageShow($item_id, 0);
            }

            ModelRUpayments::newInstance()->setColor($item_id);
        } else if ($product_type == self::SHOW_IMAGE || $product_type == self::PUBLISH_AND_SHOW_IMAGE) {
            if (ModelRUpayments::newInstance()->getIsPublishPaymentNeeded($item_id)) {
                ModelRUpayments::newInstance()->payPublishFee($item_id, $payment_id);
            }

            ModelRUpayments::newInstance()->setImageShow($item_id, 1);
        } else if ($product_type == self::SET_PREMIUM_AND_SHOW_IMAGE || $product_type == self::PUBLISH_AND_SET_PREMIUM_AND_SHOW_IMAGE) {
            if (ModelRUpayments::newInstance()->getIsPublishPaymentNeeded($item_id)) {
                ModelRUpayments::newInstance()->payPublishFee($item_id, $payment_id);
            }

            ModelRUpayments::newInstance()->setImageShow($item_id, 1);
            ModelRUpayments::newInstance()->payPremiumFee($item_id, $payment_id);
        } else if ($product_type == self::HIGHLIGHT_AND_SHOW_IMAGE || $product_type == self::PUBLISH_AND_HIGHLIGHT_AND_SHOW_IMAGE) {
            if (ModelRUpayments::newInstance()->getIsPublishPaymentNeeded($item_id)) {
                ModelRUpayments::newInstance()->payPublishFee($item_id, $payment_id);
            }

            ModelRUpayments::newInstance()->setImageShow($item_id, 1);
            ModelRUpayments::newInstance()->setColor($item_id);
        } else if ($product_type == self::SET_PREMIUM_AND_HIGHLIGHT_AND_SHOW_IMAGE || $product_type == self::PUBLISH_AND_SET_PREMIUM_AND_HIGHLIGHT_AND_SHOW_IMAGE) {
            if (ModelRUpayments::newInstance()->getIsPublishPaymentNeeded($item_id)) {
                ModelRUpayments::newInstance()->payPublishFee($item_id, $payment_id);
            }

            ModelRUpayments::newInstance()->setImageShow($item_id, 1);
            ModelRUpayments::newInstance()->payPremiumFee($item_id, $payment_id);
            ModelRUpayments::newInstance()->setColor($item_id);
        } else if ($product_type == self::PACK_3_IN_1 || $product_type == self::PUBLISH_AND_PACK_3_IN_1) {
            if (ModelRUpayments::newInstance()->getIsPublishPaymentNeeded($item_id )) {
                ModelRUpayments::newInstance()->payPublishFee($item_id, $payment_id);
            }

            if (osc_get_preference('pay_per_show_image_status', 'rupayments') && !ModelRUpayments::newInstance()->checkShowImage($item_id)) {
                ModelRUpayments::newInstance()->setImageShow($item_id, 0);
            }

            ModelRUpayments::newInstance()->setPack3in1($item_id, $payment_id);
        } else if ($product_type == self::ACTIVATE_SHOW_IMAGE_AND_PACK_3_IN_1 || $product_type == self::PUBLISH_AND_ACTIVATE_SHOW_IMAGE_AND_PACK_3_IN_1) {
            if (ModelRUpayments::newInstance()->getIsPublishPaymentNeeded($item_id)) {
                ModelRUpayments::newInstance()->payPublishFee($item_id, $payment_id);
            }

            ModelRUpayments::newInstance()->setPack3in1($item_id, $payment_id);
            ModelRUpayments::newInstance()->setImageShow($item_id, 1);
        } else if ($product_type == self::BANNER) {
            ModelRUpayments::newInstance()->setUserBannerPay($item_id);
        } else if ($product_type == self::PURCHASE) {
            ModelRUpayments::newInstance()->setEbuyDealPay($item_id, $user_id);
        } else if ($product_type == self::TO_TOP) {
            ModelRUpayments::newInstance()->setTopItem($item_id);
        } else if ($product_type == self::RENEW) {
            ModelRUpayments::newInstance()->setRenew($item_id);
        } else if ($product_type == self::HIGHLIGHT_AND_SET_PREMIUM || $product_type == self::PUBLISH_AND_HIGHLIGHT_AND_SET_PREMIUM) {
            if (ModelRUpayments::newInstance()->getIsPublishPaymentNeeded($item_id)) {
                ModelRUpayments::newInstance()->payPublishFee($item_id, $payment_id);
            }

            if (osc_get_preference('pay_per_show_image_status', 'rupayments') && !ModelRUpayments::newInstance()->checkShowImage($item_id)) {
                ModelRUpayments::newInstance()->setImageShow($item_id, 0);
            }

            ModelRUpayments::newInstance()->payPremiumWithColorFee($item_id, $payment_id);
        } else if ($product_type == self::MEMBERSHIP) {
            ModelRUpayments::newInstance()->setUserMembership($item_id, $user_id);
        } else {
            $pack = ModelRUpayments::newInstance()->getPack($item_id);

            ModelRUpayments::newInstance()->addWallet($user_id, $pack['f_pack_amount'] + $pack['f_pack_bonus']);
        }
    }

    /**
     * Страница результата оплаты
     */
    public function payment_after()
    {
        $method = Params::getParam('method');

        if ($method == 'success') {
            osc_add_flash_ok_message(__('Payment processed correctly', 'rupayments'));
        }

        if ($method == 'failure') {
            $inv_id = Params::getParam("pg_order_id");
            osc_add_flash_error_message(__("There was a problem processing your Payment $inv_id\n. Please contact the administrators",'rupayments'));
        }

        rupayments_js_redirect_to(osc_base_url());
    }

    /**
     * Проверка подписи
     *
     * @param string $sig
     * @param array $params
     * @param string $secret_key
     * @return bool
     */
    public function checkSig($sig, $script_name, $params, $secret_key)
    {
        return (string)$sig === self::makeSig($script_name, $params, $secret_key);
    }

    /**
     * Генерация подписи для запросов
     *
     * @param string $script_name
     * @param array $array
     * @param string $secret_key
     * @return string
     */
    public function makeSig($script_name, $array, $secret_key)
    {
        ksort($array);
        array_unshift($array, $script_name);
        array_push($array, $secret_key);
        $sig = implode(';', $array);
        return md5($sig);
    }

    /**
     * Вывод шаблона
     *
     * @param string $file
     * @param array $data
     * @return void
     */
    public function view($file, $data = array())
    {
        foreach ($data as $prop => $value) {
            $$prop = $value;
        }

        include osc_plugin_path(osc_plugin_folder(__FILE__)) . $file;
    }
}