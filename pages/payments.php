<?php
if ( $show_url_back ) print "<a href='".$url_back."'>".__('Back', 'rupayments')."</a>";
View::newInstance()->_exportVariableToView('item', Item::newInstance()->findByPrimaryKey($item_id));
?>
    
<link rel="stylesheet" href="<?php echo osc_base_url();?>oc-content/plugins/rupayments/css/materialdesignicons.min.css">
<div class="upay_item" id="ollpaysystem">
    <div class="upay_item_info">
        <h3><a href="<?php echo osc_item_url(); ?>"><?php echo osc_highlight( strip_tags( osc_item_title() ),25 ); ?></a></h3>
        <div class="img">
            <?php if( osc_count_item_resources() ) { ?><a href="<?php echo osc_item_url(); ?>"><img src="<?php echo osc_resource_thumbnail_url(); ?>"  alt="<?php echo osc_item_title(); ?>"/></a><?php } else { ?>
                <img src="<?php echo osc_base_url();?>oc-content/plugins/rupayments/img/no_photo.gif" alt="" title="" />
            <?php } ?>
        </div>
        <p class="bottomtext">
            <span class="mdi mdi-update mdi-18px"></span>: <?php echo osc_format_date(osc_item_pub_date()) ; ?>
            <span class="mdi mdi-cash-usd mdi-18px"></span>: <?php echo osc_format_price(osc_item_price()); ?>
        </p>
    </div>
    <div class="upay_item_service">
        <h2 class="uservicepayh2"><?php print __( "Your choice is:", "rupayments" )." ".$description." ".$image."";  ?></h2>
        <h2 class="uservicepayh2"><?php print __( "Price:", "rupayments" )." <span class='uservicepay'>".$price." ".osc_get_preference('currency', 'rupayments')."</span>";  ?></h2>
        <h3 class="uservicepayh3"><?php echo $credit_msg; ?></h3>
        <?php
        if(osc_is_web_user_logged_in()) {
            if ( isset ( $wallet['formatted_amount'] ) && (bccomp($wallet['formatted_amount'],$price,8)==1|| bccomp($wallet['formatted_amount'],$price,8)==0) ) {
                print "<div>";
                wallet_button($price, $description, $product_type."x".$item['fk_i_category_id']."x".$item['pk_i_id'], array('user' => $item['fk_i_user_id'], 'itemid' => $item['pk_i_id'], 'email' => $item['s_contact_email'] ) );
                print "</div>";
            } else { ?>
                <div></div>
                <?php
            }
        }
        ?>
        <h3 style="margin-top: 2em;"><?php _e( "You can pay with:", "rupayments" );  ?></h3>

        <div class="upay_payments">
            <form action="<?php echo osc_route_url('rupayments-premium-payments'); ?>" id="paybox_form" method="post">
                <input type="hidden" name="item_id" value="<?php echo $item_id; ?>" />
                <input type="hidden" name="product_type" value="<?php echo $product_type; ?>" />
                <input type="hidden" name="description" value="<?php echo $description; ?>" />
                <input type="hidden" name="price" value="<?php echo $price; ?>" />
                <input type="hidden" name="email" value="<?php echo $item['s_contact_email']; ?>" />
                <button type="submit" name="paybox" class="chekout udisbutton">PayBox</button>
            </form>
        </div>

        <?php if ( osc_get_preference("paypal_enabled", "rupayments") == 1 ) { ?>
            <div class="upay_payments">
                <?php Paypal::button( $price, sprintf ( $lable, $item_id, $price, osc_get_preference ( "currency", "rupayments" ), osc_page_title() ), $product_type."x".$category_id."x".$item_id, array ( 'user' => @$item['fk_i_user_id'], 'itemid' => @$item_id, 'email' => @$item['s_contact_email'] ) ); ?>
            </div>
        <?php }

        if ( osc_get_preference("co2_enabled", "rupayments") == 1 ) {?>
            <div class="upay_payments">
                <?php ModelChekout::button( $price, sprintf ( $lable, $item_id, $price, osc_get_preference ( "currency", "rupayments" ), osc_page_title() ), $item_id, $item['fk_i_user_id'], $s2CoType, __("Add funds", "rupayments"), 0 ); ?>
            </div>
        <?php  } // blockchain

        if ( osc_get_preference("blockchain_enabled", "rupayments") == 1 )  {?>
            <div class="upay_payments">
                <?php Modelblockchain::button( $price, sprintf ( $lable, $item_id, $price, osc_get_preference ( "currency", "rupayments" ), osc_page_title() ), $item_id, $item['fk_i_user_id'], $s2CoType, __("Add funds", "rupayments"), 0 ); ?>
            </div>
        <?php  }

        if ( osc_get_preference("interkassa_enabled", "rupayments") == 1 ) {?>
            <div class="upay_payments">
                <?php Interkassa::button ( $item['s_contact_email'], $price, $product_type, $item_id, $description, $site_title ); ?>
            </div>
            <?php
        }

        if ( osc_get_preference("robokassa_enabled", "rupayments") == 1 ) {?>
            <div class="upay_payments">
                <?php Robokassa::button ( $item['s_contact_email'], $price, $product_type, $item_id, $description, $site_title ); ?>
            </div>
        <?php  }

        if ( osc_get_preference("wo_enabled", "rupayments") == 1 ) {?>
            <div class="upay_payments">
                <?php Walletone::button ( $item['s_contact_email'], $price, $product_type, $item_id, $description, $site_title ); ?>
            </div>
        <?php }

        if ( osc_get_preference("payeer_enabled", "rupayments") == 1 ) {?>
            <div class="upay_payments">
                <?php Payeer::button ( $item['s_contact_email'], $price, $product_type, $item_id, $description, $site_title ); ?>
            </div>
        <?php  }

        if ( osc_get_preference("freekassa_enabled", "rupayments") == 1 ) {?>
            <div class="upay_payments">
                <?php Freekassa::button ( $item['s_contact_email'], $price, $product_type, $item_id, $description, $site_title ); ?>
            </div>
            <?php
        } ?>
        <?php

        if ( osc_get_preference("yandex_enabled", "rupayments") == 1 ) {?>
            <div class="upay_payments">
                <?php Yandex::button ( $item['s_contact_email'], $price, $product_type, $item_id, $description, 0 ); ?>
            </div>
            <?php
        }
        ?>
        <?php

        if ( osc_get_preference("webmoney_enabled", "rupayments") == 1 ) {?>
            <div class="upay_payments">
                <?php Webmoney::button ( $item['s_contact_email'], $price, $product_type, $item_id, $description, $site_title ); ?>
            </div>
            <?php
        } ?>
    </div>
</div>
<div style="clear: both;"></div>
<div name="result_div" id="result_div"></div>
<script type="text/javascript">
    var rd = document.getElementById("result_div");
</script>