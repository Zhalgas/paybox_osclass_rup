<link rel="stylesheet" href="<?php echo osc_base_url();?>oc-content/plugins/rupayments/css/materialdesignicons.min.css">

<div class="menu_ppaypal">
    <h2 class="paypal_h"><?php _e('Pay for Item Purchase', 'rupayments'); ?>: <?php echo $item['s_title']; ?></h2>
</div>

<div class="banner-publish-fee-block" style="margin-left: 0;">
    <p><strong><?php _e('Payment amount', 'rupayments') ?>:</strong> <?php echo $ebuy_item['s_item_price'] . $ebuy_item['s_item_currency']; ?></p>
</div>

<?php if ( isset ($wallet['formatted_amount']) && (bccomp($wallet['formatted_amount'],$ebuy_item['s_item_price'],8) == 1 || bccomp($wallet['formatted_amount'],$ebuy_item['s_item_price'],8) == 0) && $ebuy_item['s_item_currency'] == osc_get_preference ( "currency", "rupayments" )) :?>
    <h3 class="uservicepayh3"><?php echo $credit_msg; ?></h3>
    <div class="scrilpack">
        <?php wallet_button($ebuy_item['s_item_price'], sprintf(__("Payment of Item Purchase: %s", "rupayments"), $item['s_title']), "1001x".$ebuy_item['fk_i_ebuy_id'], array('user' => @$user['pk_i_id'], 'itemid' => $ebuy_item['fk_i_ebuy_id'], 'email' => @$user['s_email'] ) ); ?>
    </div>
<?php endif; ?>

<div class="scrilpack">
    <form action="<?php echo osc_route_url('rupayments-ebuy-payments'); ?>" id="paybox_form" method="post">
        <input type="hidden" name="item_id" value="<?php echo $ebuy_item['fk_i_ebuy_id']; ?>" />
        <input type="hidden" name="description" value="<?php echo sprintf(__("Payment of Item Purchase: %s", "rupayments"), $item['s_title']); ?>" />
        <button type="submit" name="paybox" class="chekout udisbutton">PayBox</button>
    </form>
</div>

<div class="scrilpack">
    <?php
    if ( osc_get_preference("paypal_enabled", "rupayments") == 1 ) Paypal::button( $ebuy_item['s_item_price'], sprintf(__("Payment of Item Purchase: %s", "rupayments"), $item['s_title']), '1001x'.$ebuy_item['fk_i_ebuy_id'], array('user' => @$user['pk_i_id'], 'itemid' => $ebuy_item['fk_i_ebuy_id'], 'email' => @$user['s_email']), $ebuy_item['s_item_currency']);
    ?>
</div>

<div class="scrilpack">
    <?php
    if ( osc_get_preference("co2_enabled", "rupayments") == 1 ) ModelChekout::button( $ebuy_item['s_item_price'], sprintf(__("Payment of Item Purchase: %s", "rupayments"), $item['s_title']), $ebuy_item['fk_i_ebuy_id'], $user['pk_i_id'], 17, __("Payment of Item Purchase: ", "rupayments") . $item['s_title'], $ebuy_item['fk_i_ebuy_id'], $ebuy_item['s_item_currency']);
    ?>
</div>
<div class="scrilpack">
    <?php
    if ( osc_get_preference("interkassa_enabled", "rupayments") == 1 ) Interkassa::button ( @$user['s_email'], $ebuy_item['s_item_price'], "1001", $ebuy_item['fk_i_ebuy_id'], __("Payment of Item Purchase: ", "rupayments") . $item['s_title'], '', $ebuy_item['s_item_currency'] );
    ?>
</div>
<div class="scrilpack">
    <?php
    if ( osc_get_preference("robokassa_enabled", "rupayments") == 1 ) Robokassa::button ( @$user['s_email'], $ebuy_item['s_item_price'], "1001", $ebuy_item['fk_i_ebuy_id'], __("Payment of Item Purchase: ", "rupayments") . $item['s_title'], '', $ebuy_item['s_item_currency'] );
    ?>
</div>
<div class="scrilpack">
    <?php
    if ( osc_get_preference("wo_enabled", "rupayments") == 1 ) Walletone::button ( @$user['s_email'], $ebuy_item['s_item_price'], "1001", $ebuy_item['fk_i_ebuy_id'], __("Payment of Item Purchase: ", "rupayments") . $item['s_title'], '', $ebuy_item['s_item_currency'] );
    ?>
</div>
<div class="scrilpack">
    <?php
    if ( osc_get_preference("payeer_enabled", "rupayments") == 1 ) Payeer::button ( @$user['s_email'], $ebuy_item['s_item_price'], "1001", $ebuy_item['fk_i_ebuy_id'], __("Payment of Item Purchase: ", "rupayments") . $item['s_title'], '', $ebuy_item['s_item_currency'] );
    ?>
</div>
<div class="scrilpack">
    <?php
    if ( osc_get_preference("freekassa_enabled", "rupayments") == 1 ) Freekassa::button ( @$user['s_email'], $ebuy_item['s_item_price'], "1001", $ebuy_item['fk_i_ebuy_id'], __("Payment of Item Purchase: ", "rupayments") . $item['s_title'], '', $ebuy_item['s_item_currency'] );
    ?>
</div>
<div class="scrilpack">
    <?php
    if ( osc_get_preference("blockchain_enabled", "rupayments") == 1 ) Modelblockchain::button( $ebuy_item['s_item_price'], sprintf(__("Payment of Item Purchase: %s", "rupayments"), $item['s_title']), $ebuy_item['fk_i_ebuy_id'], $user['pk_i_id'], 17, __("Payment of Item Purchase: ", "rupayments") . $item['s_title'], $ebuy_item['fk_i_ebuy_id']);
    ?>
</div>
<div class="scrilpack">
    <?php
    if ( osc_get_preference("yandex_enabled", "rupayments") == 1 ) Yandex::button ( @$user['s_email'], $ebuy_item['s_item_price'], "1001", $ebuy_item['fk_i_ebuy_id'], __("Payment of Item Purchase: ", "rupayments") . $item['s_title'], 0 );
    ?>
</div>
<div class="scrilpack">
    <?php
    if ( osc_get_preference("webmoney_enabled", "rupayments") == 1 ) Webmoney::button ( @$user['s_email'], $ebuy_item['s_item_price'], "1001", $ebuy_item['fk_i_ebuy_id'], __("Payment of Item Purchase: ", "rupayments") . $item['s_title'], '', $ebuy_item['s_item_currency'] );
    ?>
</div>