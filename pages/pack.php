<link rel="stylesheet" href="<?php echo osc_base_url();?>oc-content/plugins/rupayments/css/materialdesignicons.min.css">

<div class="rupayments">
    <h2><span class="mdi mdi-wallet mdi-24px"></span><?php echo $credit_msg; ?></h2>
</div>

<div id="ollpaysystem">
    <table cellpadding="5" cellspacing="5" style="width: 100%;">
        <?php if($packs): ?>
            <?php foreach($packs as $pack) : ?>
                <tr>
                    <td colspan="8">
                        <div class="menuwallet" style="background-color: <?php echo $pack['f_pack_color']; ?>; repeat-x scroll 0px 0px;text-align: center;"><h2><span class="mdi mdi-currency-usd mdi-24px"></span><?php echo $pack['f_pack_title']; ?> - <?php _e("Price", "rupayments");?>: <?php echo $pack['f_pack_amount'] . " " . osc_get_preference('currency', 'rupayments'); ?> <small><?php echo $pack['f_pack_description']; ?></small></h2></div>
                    </td>
                </tr>
                <tr align="center">
                    <td>
                        <div class="scrilpack">
                            <form action="<?php echo osc_route_url('rupayments-pack-payments'); ?>" id="paybox_form" method="post">
                                <input type="hidden" name="item_id" value="<?php echo $pack['fk_i_pack_id']; ?>" />
                                <input type="hidden" name="description" value="<?php echo sprintf(__("Credit for %s %s at %s", "rupayments"), $pack['f_pack_amount'], osc_get_preference("currency", "rupayments"), osc_page_title()); ?>" />
                                <button type="submit" name="paybox" class="chekout udisbutton">PayBox</button>
                            </form>
                        </div>
                        <div class="scrilpack">
                            <?php
                            if ( osc_get_preference("paypal_enabled", "rupayments") == 1 ) Paypal::button( $pack['f_pack_amount'], sprintf(__("Credit for %s %s at %s", "rupayments"), $pack['f_pack_amount'], osc_get_preference("currency", "rupayments"), osc_page_title()), '501x'.$pack['fk_i_pack_id'], array('user' => @$user['pk_i_id'], 'itemid' => @$user['pk_i_id'], 'email' => @$user['s_email']));
                            ?>
                        </div>
                        <div class="scrilpack">
                            <?php
                            if ( osc_get_preference("co2_enabled", "rupayments") == 1 ) ModelChekout::button( $pack['f_pack_amount'], sprintf(__("Credit for %s%s at %s", "rupayments"), $pack['f_pack_amount'], osc_get_preference("currency", "rupayments"), osc_page_title()), $pack['fk_i_pack_id'], $user['pk_i_id'], 6, sprintf(__("Add funds: %s", "rupayments"), $pack['f_pack_title']), $pack['fk_i_pack_id']);
                            ?>
                        </div>
                        <div class="scrilpack">
                            <?php
                            if ( osc_get_preference("interkassa_enabled", "rupayments") == 1 ) Interkassa::button ( @$user['s_email'], $pack['f_pack_amount'], "501", $pack['fk_i_pack_id'], sprintf(__("Credit for: %s", "rupayments"), $pack['f_pack_title']));
                            ?>
                        </div>
                        <div class="scrilpack">
                            <?php
                            if ( osc_get_preference("robokassa_enabled", "rupayments") == 1 ) Robokassa::button ( @$user['s_email'], $pack['f_pack_amount'], "501", $pack['fk_i_pack_id'], sprintf(__("Credit for: %s", "rupayments"), $pack['f_pack_title']) );
                            ?>
                        </div>
                        <div class="scrilpack">
                            <?php
                            if ( osc_get_preference("wo_enabled", "rupayments") == 1 ) Walletone::button ( @$user['s_email'], $pack['f_pack_amount'], "501", $pack['fk_i_pack_id'], sprintf(__("Credit for: %s", "rupayments"), $pack['f_pack_title']) );
                            ?>
                        </div>
                        <div class="scrilpack">
                            <?php
                            if ( osc_get_preference("payeer_enabled", "rupayments") == 1 ) Payeer::button ( @$user['s_email'], $pack['f_pack_amount'], "501", $pack['fk_i_pack_id'], sprintf(__("Credit for: %s", "rupayments"), $pack['f_pack_title']));
                            ?>
                        </div>
                        <div class="scrilpack">
                            <?php
                            if ( osc_get_preference("freekassa_enabled", "rupayments") == 1 ) Freekassa::button ( @$user['s_email'], $pack['f_pack_amount'], "501", $pack['fk_i_pack_id'], sprintf(__("Credit for: %s", "rupayments"), $pack['f_pack_title']));
                            ?>
                        </div>

                        <div class="scrilpack">
                            <?php
                            if ( osc_get_preference("blockchain_enabled", "rupayments") == 1 ) Modelblockchain::button( $pack['f_pack_amount'], sprintf(__("Credit for %s%s at %s", "rupayments"), $pack['f_pack_amount'], osc_get_preference("currency", "rupayments"), osc_page_title()), $pack['fk_i_pack_id'], $user['pk_i_id'], 6, sprintf(__("Add funds: %s", "rupayments"), $pack['f_pack_title']), $pack['fk_i_pack_id']);
                            ?>
                        </div>
                        <div class="scrilpack">
                            <?php
                            if ( osc_get_preference("yandex_enabled", "rupayments") == 1 ) Yandex::button ( @$user['s_email'], $pack['f_pack_amount'], "501", $pack['fk_i_pack_id'], sprintf(__("Credit for: %s", "rupayments"), $pack['f_pack_title']), $pack['fk_i_pack_id'] );
                            ?>
                        </div>
                        <div class="scrilpack">
                            <?php
                            if ( osc_get_preference("webmoney_enabled", "rupayments") == 1 ) Webmoney::button ( @$user['s_email'], $pack['f_pack_amount'], "501", $pack['fk_i_pack_id'], sprintf(__("Credit for: %s", "rupayments"), $pack['f_pack_title']));
                            ?>
                        </div>
                        <?php
                        if ( osc_get_preference("fortumo_enabled", "rupayments") == 1 ){?>
                            <div class="menuwallet" style="background-color: #02baab; repeat-x scroll 0px 0px;text-align: center;"><h2><?php _e("SMS", "rupayments");?></h2></div>
                            <div class="rupayments">
                                <?php
                                Modelfortumo::button( $pack['f_pack_amount'], sprintf(__("Credit for %s%s at %s", "rupayments"), $pack['f_pack_amount'], osc_get_preference("currency", "rupayments"), osc_page_title()), $pack['fk_i_pack_id'], $user['pk_i_id'], 6, sprintf(__("Add funds: %s", "rupayments"), $pack['f_pack_title']), $pack['fk_i_pack_id']);  ?>
                            </div>
                        <?php } ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </table>
    <br />
</div>
<div name="result_div" id="result_div"></div>
<style>small{font-size: 12px;}</style>
<script type="text/javascript">
    var rd = document.getElementById("result_div");
</script>