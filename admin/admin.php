<?php defined('ABS_PATH') or die('Access denied');

if (Params::getParam('plugin_action')=='done') {
    osc_set_preference('test_mode', Params::getParam("test_mode") ? Params::getParam("test_mode") : '0', 'paybox_rupayment', 'BOOLEAN');
    osc_set_preference('merchant_id', Params::getParam("merchant_id") ? Params::getParam("merchant_id") : '0', 'paybox_rupayment', 'INTEGER');
    osc_set_preference('secret_key', Params::getParam("secret_key") ? Params::getParam("secret_key") : '0', 'paybox_rupayment', 'STRING');
    osc_set_preference('lifetime', Params::getParam("lifetime") ? Params::getParam("lifetime") : '0', 'paybox_rupayment', 'INTEGER');
    ob_get_clean();
    osc_add_flash_ok_message(__('Congratulations, the plugin is now configured', 'paybox_rupayments'), 'admin');
    osc_redirect_to(osc_route_admin_url('pbrp-menu'));
}

?>
<link rel="stylesheet" href="<?php echo osc_base_url();?>/oc-content/plugins/paybox_rupayments/admin/css/admin.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<h2 class="render-title"><b><i class="fa fa-cog"></i> <?php _e('Paybox settings', 'paybox_rupayments'); ?></b></h2>
<form action="<?php osc_admin_base_url(true); ?>" method="post">
    <input type="hidden" name="page" value="plugins" />
    <input type="hidden" name="action" value="renderplugin" />
    <input type="hidden" name="route" value="pbrp-menu" />
    <input type="hidden" name="plugin_action" value="done" />

    <div>
        <div class="form-row">
            <div class="form-controls">
                <div class="form-label">
                    <label>
                        <input type="text" name="merchant_id" value="<?php echo (osc_get_preference('merchant_id', 'paybox_rupayment') ? osc_get_preference('merchant_id', 'paybox_rupayment') : ''); ?>" />
                        <?php _e('Merchant ID', 'paybox_rupayments'); ?>
                    </label>
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="form-controls">
                <div class="form-label">
                    <label>
                        <input type="text" name="secret_key" value="<?php echo (osc_get_preference('secret_key', 'paybox_rupayment') ? osc_get_preference('secret_key', 'paybox_rupayment') : ''); ?>" />
                        <?php _e('Secret key', 'paybox_rupayments'); ?>
                    </label>
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="form-controls">
                <div class="form-label">
                    <label>
                        <input type="text" name="lifetime" value="<?php echo (osc_get_preference('lifetime', 'paybox_rupayment') ? osc_get_preference('lifetime', 'paybox_rupayment') : ''); ?>" />
                        <?php _e('Lifetime', 'paybox_rupayments'); ?>
                    </label>
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="form-controls">
                <div class="form-label-checkbox">
                    <label>
                        <input type="checkbox" <?php echo (osc_get_preference('test_mode', 'paybox_rupayment') ? 'checked="true"' : ''); ?> name="test_mode" value="1" />
                        <?php _e('Test mode', 'paybox_rupayments'); ?>
                    </label>
                </div>
            </div>
        </div>
        <p>
            <div class="form-actions">
                <input type="submit" id="save_changes" value="<?php echo osc_esc_html(__("Save", 'paybox_rupayments')); ?>" class="btn btn-submit">
            </div>
        </p>
    </div>
</form>