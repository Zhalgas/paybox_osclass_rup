<link rel="stylesheet" href="<?php echo osc_base_url();?>oc-content/plugins/rupayments/css/materialdesignicons.min.css">

<div class="menu_ppaypal">
    <h2 class="paypal_h"><?php _e('Pay for Banner Public', 'rupayments'); ?></h2>
</div>

<div class="banner-publish-fee-block" style="margin-left: 0;">
    <p><strong><?php _e('Payment amount', 'rupayments') ?>:</strong> <?php echo $get_banner['i_banner_budget'] . osc_get_preference('currency', 'rupayments'); ?></p>
</div>

<?php if ( isset ($wallet['formatted_amount']) && (bccomp($wallet['formatted_amount'],$get_banner['i_banner_budget'],8) == 1 || bccomp($wallet['formatted_amount'],$get_banner['i_banner_budget'],8) == 0)) :?>
    <h3 class="uservicepayh3"><?php echo $credit_msg; ?></h3>
    <div class="scrilpack">
        <?php wallet_button($get_banner['i_banner_budget'], sprintf(__("Payment of banner public fee: %s", "rupayments"), $get_banner['fk_i_banner_id']), "901x".$get_banner['fk_i_banner_id'], array('user' => @$user['pk_i_id'], 'itemid' => $get_banner['fk_i_banner_id'], 'email' => @$user['s_email'] ) ); ?>
    </div>
<?php endif; ?>

<div class="scrilpack">
    <form action="<?php echo osc_route_url('rupayments-banner-payments'); ?>" id="paybox_form" method="post">
        <input type="hidden" name="banner_id" value="<?php echo $get_banner['fk_i_banner_id']; ?>" />
        <input type="hidden" name="description" value="<?php echo sprintf(__("Payment of banner public fee: %s", "rupayments"), $get_banner['fk_i_banner_id']); ?>" />
        <button type="submit" name="paybox" class="chekout udisbutton">PayBox</button>
    </form>
</div>

<div class="scrilpack">
    <?php
    if ( osc_get_preference("paypal_enabled", "rupayments") == 1 ) Paypal::button( $get_banner['i_banner_budget'], sprintf(__("Payment of banner public fee: %s", "rupayments"), $get_banner['fk_i_banner_id']), '901x'.$get_banner['fk_i_banner_id'], array('user' => @$user['pk_i_id'], 'itemid' => $get_banner['fk_i_banner_id'], 'email' => @$user['s_email']));
    ?>
</div>

<div class="scrilpack">
    <?php
    if ( osc_get_preference("co2_enabled", "rupayments") == 1 ) ModelChekout::button( $get_banner['i_banner_budget'], sprintf(__("Payment of banner public fee: %s", "rupayments"), $get_banner['fk_i_banner_id']), $get_banner['fk_i_banner_id'], $user['pk_i_id'], 16,  sprintf(__("Payment of banner public fee: %s", "rupayments"), $get_banner['fk_i_banner_id']), $get_banner['fk_i_banner_id']);
    ?>
</div>

<div class="scrilpack">
    <?php
    if ( osc_get_preference("interkassa_enabled", "rupayments") == 1 ) Interkassa::button (  @$user['s_email'], $get_banner['i_banner_budget'], "901", $get_banner['fk_i_banner_id'],  sprintf(__("Payment of banner public fee: %s", "rupayments"), $get_banner['fk_i_banner_id']) );
    ?>
</div>
<div class="scrilpack">
    <?php
    if ( osc_get_preference("robokassa_enabled", "rupayments") == 1 ) Robokassa::button ( @$user['s_email'], $get_banner['i_banner_budget'], "901", $get_banner['fk_i_banner_id'],  sprintf(__("Payment of banner public fee: %s", "rupayments"), $get_banner['fk_i_banner_id']) );
    ?>
</div>
<div class="scrilpack">
    <?php
    if ( osc_get_preference("wo_enabled", "rupayments") == 1 ) Walletone::button (  @$user['s_email'], $get_banner['i_banner_budget'], "901", $get_banner['fk_i_banner_id'],  sprintf(__("Payment of banner public fee: %s", "rupayments"), $get_banner['fk_i_banner_id']) );
    ?>
</div>
<div class="scrilpack">
    <?php
    if ( osc_get_preference("payeer_enabled", "rupayments") == 1 ) Payeer::button ( @$user['s_email'], $get_banner['i_banner_budget'], "901", $get_banner['fk_i_banner_id'],  sprintf(__("Payment of banner public fee: %s", "rupayments"), $get_banner['fk_i_banner_id']) );
    ?>
</div>
<div class="scrilpack">
    <?php
    if ( osc_get_preference("freekassa_enabled", "rupayments") == 1 ) Freekassa::button (  @$user['s_email'], $get_banner['i_banner_budget'], "901", $get_banner['fk_i_banner_id'],  sprintf(__("Payment of banner public fee: %s", "rupayments"), $get_banner['fk_i_banner_id']) );
    ?>
</div>
<div class="scrilpack">
    <?php
    if ( osc_get_preference("blockchain_enabled", "rupayments") == 1 ) Modelblockchain::button( $get_banner['i_banner_budget'], sprintf(__("Payment of banner public fee: %s", "rupayments"), $get_banner['fk_i_banner_id']), $get_banner['fk_i_banner_id'], $user['pk_i_id'], 16,  sprintf(__("Payment of banner public fee: %s", "rupayments"), $get_banner['fk_i_banner_id']), $get_banner['fk_i_banner_id']);
    ?>
</div>
<div class="scrilpack">
    <?php
    if ( osc_get_preference("yandex_enabled", "rupayments") == 1 ) Yandex::button (  @$user['s_email'], $get_banner['i_banner_budget'], "901", $get_banner['fk_i_banner_id'],  sprintf(__("Payment of banner public fee: %s", "rupayments"), $get_banner['fk_i_banner_id']), 0  );
    ?>
</div>
<div class="scrilpack">
    <?php
    if ( osc_get_preference("webmoney_enabled", "rupayments") == 1 ) Webmoney::button (  @$user['s_email'], $get_banner['i_banner_budget'], "901", $get_banner['fk_i_banner_id'],  sprintf(__("Payment of banner public fee: %s", "rupayments"), $get_banner['fk_i_banner_id']) );
    ?>
</div>